# CCE集群环境下多租设计简介<a name="ZH-CN_TOPIC_0000001349267629"></a>

如果已经规划好了您的SaaS应用多租和开发内容，那是时候考虑SaaS应用部署环境的问题，在SaaS应用场景，线下IDC部署显然不是一个很好的选择，相较于基于云上虚拟机部署模式，采用Kubernetes集群模式部署SaaS应用应该是首选，云容器引擎CCE提供高度可扩展的、高性能的企业级Kubernetes集群，充分利用[云上弹性能力](https://support.huaweicloud.com/bestpractice-cce/cce_bestpractice_00282.html)、丰富存储类型，支持实现SaaS业务降成本、动态扩容、高可靠性等需求。云容器引擎CCE可以为云上构建SaaS应用提供不同多租隔离模式，如集群隔离、namespace隔离、pod隔离等。

