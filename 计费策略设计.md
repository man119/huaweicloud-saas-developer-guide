# 计费策略设计<a name="ZH-CN_TOPIC_0000001321576393"></a>

SaaS应用一般采用出租方式收费，而不是像传统软件那样一次性买断。那么对于SaaS厂家而言，针对不同的产品业务、客户群体、市场定位和商业策略，制定出最合适的计费策略则显示尤为重要。计费策略主要围绕以下3个方面来制定：

-   用户数
-   功能/服务
-   资源使用量（存储量、调用次数等）

在确定SaaS应用的计费策略之前，需要先考虑如下关键信息：

-   明确公司的阶段目标：现阶段是提升市占率，还是企业利润最大化？
-   明确价格的底线：在考虑成本时，研发投入等变动成本可以不考虑，但销售团队的新购增购销售提成、管理提成、绩效奖金、实施成本和服务成本等要考虑进来。即使以市占率为主要目标，“销售团队正毛利”还是价格的底线。
-   参考市场上同类产品的价格，主要有如下两种：
    -   如果SaaS产品要与传统软件竞争，可以考虑采用同类传统软件1/3左右的价格作为年度租用价格。因为传统软件是一次支付为主，而SaaS可以多年收下去。
    -   如果该领域同类软件有国际品牌，同样可以锚定标杆品牌制定本土定价。

-   SaaS产品要设法展示差异，根据产品及服务的差异选择合适价格。

同时，也需要考虑影响盈利的几个关键因素：

-   华为云的计费策略：华为云的计费策略会影响SaaS应用的盈利水平，需要根据华为云或第三方服务的计费策略，制定SaaS应用对应的计费策略。
-   SaaS应用使用场景：针对用户使用SaaS应用的场景，降低整体成本。例如用户只有少部分时间使用SaaS应用，是否可以制定相应的策略来有效降低成本，提升盈利。
-   存储配额或数据保留期：随着时间的推移，数据量持续上升，需要消耗更多的存储空间，同时保护数据的成本也更高，从而降低每个租户的盈利率。需要考虑在计费策略中是否可以设置存储配额或数据保留期？
-   服务限制：对于某些计费策略（例如统一费率计费），如果租户之间共享资源，需要考虑租户过度使用资源导致其他租户性能下降情况，此时需要对服务进行限制，否则可能无法盈利。
-   租户对服务级别的要求：需要清楚地了解租户对服务级别的期望和SaaS厂商需要承担的义务，以便制定适合的计费策略，否则可能意味着SaaS应用出现亏损。

## 常用计费策略<a name="zh-cn_topic_0000001258663947_section1829117663219"></a>

为帮助开发者提升盈利水平，现根据不同的租户类型、营销策略制定如下常用计费策略，请开发者结合实际情况选择合适的计费策略。

**表 1**  计费策略说明

<a name="zh-cn_topic_0000001258663947_table944849135016"></a>
<table><thead align="left"><tr id="zh-cn_topic_0000001258663947_row94454910507"><th class="cellrowborder" valign="top" width="18.29%" id="mcps1.2.5.1.1"><p id="zh-cn_topic_0000001258663947_p3451249185014"><a name="zh-cn_topic_0000001258663947_p3451249185014"></a><a name="zh-cn_topic_0000001258663947_p3451249185014"></a>计费策略</p>
</th>
<th class="cellrowborder" valign="top" width="31.66%" id="mcps1.2.5.1.2"><p id="zh-cn_topic_0000001258663947_p44534915013"><a name="zh-cn_topic_0000001258663947_p44534915013"></a><a name="zh-cn_topic_0000001258663947_p44534915013"></a>计费策略说明</p>
</th>
<th class="cellrowborder" valign="top" width="25.05%" id="mcps1.2.5.1.3"><p id="zh-cn_topic_0000001258663947_p16451449115013"><a name="zh-cn_topic_0000001258663947_p16451449115013"></a><a name="zh-cn_topic_0000001258663947_p16451449115013"></a>优点</p>
</th>
<th class="cellrowborder" valign="top" width="25%" id="mcps1.2.5.1.4"><p id="zh-cn_topic_0000001258663947_p1451749105011"><a name="zh-cn_topic_0000001258663947_p1451749105011"></a><a name="zh-cn_topic_0000001258663947_p1451749105011"></a>缺点</p>
</th>
</tr>
</thead>
<tbody><tr id="zh-cn_topic_0000001258663947_row5454496507"><td class="cellrowborder" valign="top" width="18.29%" headers="mcps1.2.5.1.1 "><p id="zh-cn_topic_0000001258663947_p13451549135010"><a name="zh-cn_topic_0000001258663947_p13451549135010"></a><a name="zh-cn_topic_0000001258663947_p13451549135010"></a>按需计费</p>
</td>
<td class="cellrowborder" valign="top" width="31.66%" headers="mcps1.2.5.1.2 "><p id="zh-cn_topic_0000001258663947_p24544910506"><a name="zh-cn_topic_0000001258663947_p24544910506"></a><a name="zh-cn_topic_0000001258663947_p24544910506"></a>根据租户使用的资源量来计算费用，多用多付费，不用则不需要付费。</p>
<p id="zh-cn_topic_0000001258663947_p2306142419456"><a name="zh-cn_topic_0000001258663947_p2306142419456"></a><a name="zh-cn_topic_0000001258663947_p2306142419456"></a>适用于：</p>
<p id="zh-cn_topic_0000001258663947_p0466113019454"><a name="zh-cn_topic_0000001258663947_p0466113019454"></a><a name="zh-cn_topic_0000001258663947_p0466113019454"></a>价格比较敏感的客户，希望按照使用情况付费。</p>
</td>
<td class="cellrowborder" valign="top" width="25.05%" headers="mcps1.2.5.1.3 "><a name="zh-cn_topic_0000001258663947_ul16396935183513"></a><a name="zh-cn_topic_0000001258663947_ul16396935183513"></a><ul id="zh-cn_topic_0000001258663947_ul16396935183513"><li>租户前期投入少，容易推广。</li><li>当开发者使用的华为云也是按需计费时，则SaaS应用也很适合用按需计费策略。</li></ul>
</td>
<td class="cellrowborder" valign="top" width="25%" headers="mcps1.2.5.1.4 "><a name="zh-cn_topic_0000001258663947_ul9568153913351"></a><a name="zh-cn_topic_0000001258663947_ul9568153913351"></a><ul id="zh-cn_topic_0000001258663947_ul9568153913351"><li>前期很难准确评估收入。</li><li>由于少用则少付费，会导致租户为降低成本而尽量少使用资源。</li><li>由于是按需计费，需要准确计算租户对资源的使用情况，当租户消耗资源持续增加时，准确计算费用的难度会上升。</li></ul>
</td>
</tr>
<tr id="zh-cn_topic_0000001258663947_row94614955019"><td class="cellrowborder" valign="top" width="18.29%" headers="mcps1.2.5.1.1 "><p id="zh-cn_topic_0000001258663947_p346204913508"><a name="zh-cn_topic_0000001258663947_p346204913508"></a><a name="zh-cn_topic_0000001258663947_p346204913508"></a>按用户数量计费</p>
</td>
<td class="cellrowborder" valign="top" width="31.66%" headers="mcps1.2.5.1.2 "><p id="zh-cn_topic_0000001258663947_p04604913505"><a name="zh-cn_topic_0000001258663947_p04604913505"></a><a name="zh-cn_topic_0000001258663947_p04604913505"></a>为每个用户约定固定的费用，一旦费用确定后，不随用户使用资源量而改变费用。</p>
<p id="zh-cn_topic_0000001258663947_p1182761194310"><a name="zh-cn_topic_0000001258663947_p1182761194310"></a><a name="zh-cn_topic_0000001258663947_p1182761194310"></a>适用于：</p>
<a name="zh-cn_topic_0000001258663947_ul1302184084416"></a><a name="zh-cn_topic_0000001258663947_ul1302184084416"></a><ul id="zh-cn_topic_0000001258663947_ul1302184084416"><li>整个公司都需要使用的产品</li><li>每个用户都需要独立使用的产品</li></ul>
</td>
<td class="cellrowborder" valign="top" width="25.05%" headers="mcps1.2.5.1.3 "><a name="zh-cn_topic_0000001258663947_ul6986113519119"></a><a name="zh-cn_topic_0000001258663947_ul6986113519119"></a><ul id="zh-cn_topic_0000001258663947_ul6986113519119"><li>由于每个用户的费用固定，可以准确评估收入。</li><li>随着用户数量的增加，可以获多更多的收入。</li><li>由于每用户费用统一，降低推广难度。</li></ul>
<p id="zh-cn_topic_0000001258663947_p2326104164613"><a name="zh-cn_topic_0000001258663947_p2326104164613"></a><a name="zh-cn_topic_0000001258663947_p2326104164613"></a></p>
</td>
<td class="cellrowborder" valign="top" width="25%" headers="mcps1.2.5.1.4 "><a name="zh-cn_topic_0000001258663947_ul198181538417"></a><a name="zh-cn_topic_0000001258663947_ul198181538417"></a><ul id="zh-cn_topic_0000001258663947_ul198181538417"><li>由于费用固定，用户使用资源量越大时，则盈利水平越低。</li><li>需要记录每个用户的资源使用量，以便标识出资源使用量大的用户，这会增加系统的负担。</li><li>由于增加用户数会增加费用，对于人员较多的租户，会导致多人共用一个帐号的情况。</li></ul>
</td>
</tr>
<tr id="zh-cn_topic_0000001258663947_row54664915507"><td class="cellrowborder" valign="top" width="18.29%" headers="mcps1.2.5.1.1 "><p id="zh-cn_topic_0000001258663947_p1246144915503"><a name="zh-cn_topic_0000001258663947_p1246144915503"></a><a name="zh-cn_topic_0000001258663947_p1246144915503"></a>按活跃用户数量计费</p>
</td>
<td class="cellrowborder" valign="top" width="31.66%" headers="mcps1.2.5.1.2 "><p id="zh-cn_topic_0000001258663947_p107238556128"><a name="zh-cn_topic_0000001258663947_p107238556128"></a><a name="zh-cn_topic_0000001258663947_p107238556128"></a>在一个时间段内，比如以月为周期，统计活跃用户数量，然后根据活跃用户量进行收费。</p>
<p id="zh-cn_topic_0000001258663947_p2046134975010"><a name="zh-cn_topic_0000001258663947_p2046134975010"></a><a name="zh-cn_topic_0000001258663947_p2046134975010"></a>该收费方式跟“按用户数量计费”类似。</p>
<p id="zh-cn_topic_0000001258663947_p91685818456"><a name="zh-cn_topic_0000001258663947_p91685818456"></a><a name="zh-cn_topic_0000001258663947_p91685818456"></a>适用于：</p>
<a name="zh-cn_topic_0000001258663947_ul616818834519"></a><a name="zh-cn_topic_0000001258663947_ul616818834519"></a><ul id="zh-cn_topic_0000001258663947_ul616818834519"><li>整个公司都需要使用的产品</li><li>每个用户都需要独立使用的产品</li></ul>
</td>
<td class="cellrowborder" valign="top" width="25.05%" headers="mcps1.2.5.1.3 "><a name="zh-cn_topic_0000001258663947_ul36692719918"></a><a name="zh-cn_topic_0000001258663947_ul36692719918"></a><ul id="zh-cn_topic_0000001258663947_ul36692719918"><li>租户前期投入少，容易推广。</li><li>随着活跃用户数量的增加，可以获多更多的收入。</li></ul>
</td>
<td class="cellrowborder" valign="top" width="25%" headers="mcps1.2.5.1.4 "><a name="zh-cn_topic_0000001258663947_ul1216415161897"></a><a name="zh-cn_topic_0000001258663947_ul1216415161897"></a><ul id="zh-cn_topic_0000001258663947_ul1216415161897"><li>前期很难准确评估收入。</li><li>由于费用固定，用户使用资源量越大时，则盈利水平越低。</li><li>需要和租户进行协商，设置一个资源使用量的阈值，超过这个值的用户才被认定为活跃用户。</li></ul>
</td>
</tr>
<tr id="zh-cn_topic_0000001258663947_row3669191315535"><td class="cellrowborder" valign="top" width="18.29%" headers="mcps1.2.5.1.1 "><p id="zh-cn_topic_0000001258663947_p1466941345312"><a name="zh-cn_topic_0000001258663947_p1466941345312"></a><a name="zh-cn_topic_0000001258663947_p1466941345312"></a>按单位数量计费</p>
</td>
<td class="cellrowborder" valign="top" width="31.66%" headers="mcps1.2.5.1.2 "><p id="zh-cn_topic_0000001258663947_p1166941313538"><a name="zh-cn_topic_0000001258663947_p1166941313538"></a><a name="zh-cn_topic_0000001258663947_p1166941313538"></a>为每个单位或设备约定固定的费用，一旦费用确定后，不随单位或设备使用资源量而改变费用。当资源的消耗不是基于用户，而是基于设备或单位时，比如物联网场景，则建议使用该计费策略。</p>
<p id="zh-cn_topic_0000001258663947_p581975112509"><a name="zh-cn_topic_0000001258663947_p581975112509"></a><a name="zh-cn_topic_0000001258663947_p581975112509"></a>适用于：</p>
<p id="zh-cn_topic_0000001258663947_p1588955465020"><a name="zh-cn_topic_0000001258663947_p1588955465020"></a><a name="zh-cn_topic_0000001258663947_p1588955465020"></a>物联网场景客户</p>
</td>
<td class="cellrowborder" valign="top" width="25.05%" headers="mcps1.2.5.1.3 "><a name="zh-cn_topic_0000001258663947_ul165431154173518"></a><a name="zh-cn_topic_0000001258663947_ul165431154173518"></a><ul id="zh-cn_topic_0000001258663947_ul165431154173518"><li>由于每个单位或设备的费用固定，可以准确评估收入。</li><li>计费简单，后续用户量增加时，计费难度不会上升。</li></ul>
</td>
<td class="cellrowborder" valign="top" width="25%" headers="mcps1.2.5.1.4 "><a name="zh-cn_topic_0000001258663947_ul0185121473614"></a><a name="zh-cn_topic_0000001258663947_ul0185121473614"></a><ul id="zh-cn_topic_0000001258663947_ul0185121473614"><li>由于费用固定，单位或设备使用资源量越大时，则盈利水平越低。</li><li>需要记录每个单位或设备的资源使用量，以便标识出资源使用量大的单位或设备，这会增加系统的负担。</li></ul>
</td>
</tr>
<tr id="zh-cn_topic_0000001258663947_row7144471566"><td class="cellrowborder" valign="top" width="18.29%" headers="mcps1.2.5.1.1 "><p id="zh-cn_topic_0000001258663947_p191414713568"><a name="zh-cn_topic_0000001258663947_p191414713568"></a><a name="zh-cn_topic_0000001258663947_p191414713568"></a>按功能计费</p>
</td>
<td class="cellrowborder" valign="top" width="31.66%" headers="mcps1.2.5.1.2 "><p id="zh-cn_topic_0000001258663947_p201494735619"><a name="zh-cn_topic_0000001258663947_p201494735619"></a><a name="zh-cn_topic_0000001258663947_p201494735619"></a>将功能进行分层，例如基础版、高级版等，不同的层级对应的产品功能不同，费用也不同。</p>
<p id="zh-cn_topic_0000001258663947_p977810634719"><a name="zh-cn_topic_0000001258663947_p977810634719"></a><a name="zh-cn_topic_0000001258663947_p977810634719"></a>适用于：</p>
<p id="zh-cn_topic_0000001258663947_p19665131311478"><a name="zh-cn_topic_0000001258663947_p19665131311478"></a><a name="zh-cn_topic_0000001258663947_p19665131311478"></a>目标客户对产品功能需求不同</p>
</td>
<td class="cellrowborder" valign="top" width="25.05%" headers="mcps1.2.5.1.3 "><a name="zh-cn_topic_0000001258663947_ul18254161010215"></a><a name="zh-cn_topic_0000001258663947_ul18254161010215"></a><ul id="zh-cn_topic_0000001258663947_ul18254161010215"><li>对于只需要使用基础功能的用户，前期投入小，容易推广。</li><li>由于每个用户的费用固定，可以准确评估收入。</li><li>用户需要升级才能使用更多的功能，具有强烈升级的愿望。</li></ul>
</td>
<td class="cellrowborder" valign="top" width="25%" headers="mcps1.2.5.1.4 "><a name="zh-cn_topic_0000001258663947_ul8545121420215"></a><a name="zh-cn_topic_0000001258663947_ul8545121420215"></a><ul id="zh-cn_topic_0000001258663947_ul8545121420215"><li>前期需要精准分析用户对产品功能的需求，才能更好的对产品功能进行分层。</li><li>如果分层太多，会造成定价困难，并且用户也会难以选择合适的层级。</li></ul>
<p id="zh-cn_topic_0000001258663947_p169199135012"><a name="zh-cn_topic_0000001258663947_p169199135012"></a><a name="zh-cn_topic_0000001258663947_p169199135012"></a></p>
</td>
</tr>
<tr id="zh-cn_topic_0000001258663947_row11400104812564"><td class="cellrowborder" valign="top" width="18.29%" headers="mcps1.2.5.1.1 "><p id="zh-cn_topic_0000001258663947_p540034814567"><a name="zh-cn_topic_0000001258663947_p540034814567"></a><a name="zh-cn_topic_0000001258663947_p540034814567"></a>有限免费服务计费</p>
</td>
<td class="cellrowborder" valign="top" width="31.66%" headers="mcps1.2.5.1.2 "><p id="zh-cn_topic_0000001258663947_p94001348195610"><a name="zh-cn_topic_0000001258663947_p94001348195610"></a><a name="zh-cn_topic_0000001258663947_p94001348195610"></a>有限免费服务计费包括如下两方面：</p>
<a name="zh-cn_topic_0000001258663947_ul0136143344318"></a><a name="zh-cn_topic_0000001258663947_ul0136143344318"></a><ul id="zh-cn_topic_0000001258663947_ul0136143344318"><li>免费试用：将部分或全部功能免费给用户试用一段时间，然后开始收费。</li><li>基础功能永久免费：将基础功能永久免费开放，当用户需要使用高级功能时，开始收费。</li></ul>
<p id="zh-cn_topic_0000001258663947_p676243920492"><a name="zh-cn_topic_0000001258663947_p676243920492"></a><a name="zh-cn_topic_0000001258663947_p676243920492"></a>适用于：</p>
<a name="zh-cn_topic_0000001258663947_ul14443241175018"></a><a name="zh-cn_topic_0000001258663947_ul14443241175018"></a><ul id="zh-cn_topic_0000001258663947_ul14443241175018"><li>需要快速提升市占率的产品</li><li>针对预算比较有限的客户</li></ul>
</td>
<td class="cellrowborder" valign="top" width="25.05%" headers="mcps1.2.5.1.3 "><p id="zh-cn_topic_0000001258663947_p34008480563"><a name="zh-cn_topic_0000001258663947_p34008480563"></a><a name="zh-cn_topic_0000001258663947_p34008480563"></a>租户前期免费使用，更容易推广，可以快速提升市占率。</p>
<p id="zh-cn_topic_0000001258663947_p2956601454"><a name="zh-cn_topic_0000001258663947_p2956601454"></a><a name="zh-cn_topic_0000001258663947_p2956601454"></a></p>
</td>
<td class="cellrowborder" valign="top" width="25%" headers="mcps1.2.5.1.4 "><a name="zh-cn_topic_0000001258663947_ul2637151115220"></a><a name="zh-cn_topic_0000001258663947_ul2637151115220"></a><ul id="zh-cn_topic_0000001258663947_ul2637151115220"><li>前期很难准确评估收入。</li><li>如果免费用户太多，将影响盈利。</li></ul>
</td>
</tr>
<tr id="zh-cn_topic_0000001258663947_row136734910562"><td class="cellrowborder" valign="top" width="18.29%" headers="mcps1.2.5.1.1 "><p id="zh-cn_topic_0000001258663947_p96714499564"><a name="zh-cn_topic_0000001258663947_p96714499564"></a><a name="zh-cn_topic_0000001258663947_p96714499564"></a>按成本计费</p>
</td>
<td class="cellrowborder" valign="top" width="31.66%" headers="mcps1.2.5.1.2 "><p id="zh-cn_topic_0000001258663947_p183714436248"><a name="zh-cn_topic_0000001258663947_p183714436248"></a><a name="zh-cn_topic_0000001258663947_p183714436248"></a>根据租户使用的资源量来计算费用，但只按资源的成本价收费，没有盈利。</p>
<p id="zh-cn_topic_0000001258663947_p76501358144117"><a name="zh-cn_topic_0000001258663947_p76501358144117"></a><a name="zh-cn_topic_0000001258663947_p76501358144117"></a>该收费方式跟“按需计费”类似。</p>
<p id="zh-cn_topic_0000001258663947_p1634884194220"><a name="zh-cn_topic_0000001258663947_p1634884194220"></a><a name="zh-cn_topic_0000001258663947_p1634884194220"></a>适用于：</p>
<a name="zh-cn_topic_0000001258663947_ul146371748114210"></a><a name="zh-cn_topic_0000001258663947_ul146371748114210"></a><ul id="zh-cn_topic_0000001258663947_ul146371748114210"><li>内部用户</li><li>非盈利产品</li></ul>
</td>
<td class="cellrowborder" valign="top" width="25.05%" headers="mcps1.2.5.1.3 "><a name="zh-cn_topic_0000001258663947_ul1090693911349"></a><a name="zh-cn_topic_0000001258663947_ul1090693911349"></a><ul id="zh-cn_topic_0000001258663947_ul1090693911349"><li>租户前期投入少且按产品成本价进行付费，更容易推广。</li><li>由于少用则少付费，会导致租户为降低成本而尽量少使用资源，但由于是内部用户或者非盈利产品，所以不会造成利润不足的问题。</li></ul>
</td>
<td class="cellrowborder" valign="top" width="25%" headers="mcps1.2.5.1.4 "><p id="zh-cn_topic_0000001258663947_p967194945610"><a name="zh-cn_topic_0000001258663947_p967194945610"></a><a name="zh-cn_topic_0000001258663947_p967194945610"></a>由于是按需计费，需要准确计算租户对资源的使用情况，当租户消耗资源持续增加时，准确计算费用的难度会上升。</p>
</td>
</tr>
<tr id="zh-cn_topic_0000001258663947_row97191949155613"><td class="cellrowborder" valign="top" width="18.29%" headers="mcps1.2.5.1.1 "><p id="zh-cn_topic_0000001258663947_p471994925611"><a name="zh-cn_topic_0000001258663947_p471994925611"></a><a name="zh-cn_topic_0000001258663947_p471994925611"></a>按统一费率计费</p>
</td>
<td class="cellrowborder" valign="top" width="31.66%" headers="mcps1.2.5.1.2 "><p id="zh-cn_topic_0000001258663947_p1871944918569"><a name="zh-cn_topic_0000001258663947_p1871944918569"></a><a name="zh-cn_topic_0000001258663947_p1871944918569"></a>在一个时间段内，比如以月为周期，无论租户消耗了多少资源、有多少用户数量或使用了多少服务，都对所有租户统一收费。</p>
<p id="zh-cn_topic_0000001258663947_p589813316404"><a name="zh-cn_topic_0000001258663947_p589813316404"></a><a name="zh-cn_topic_0000001258663947_p589813316404"></a>适用于：</p>
<a name="zh-cn_topic_0000001258663947_ul1643764417417"></a><a name="zh-cn_topic_0000001258663947_ul1643764417417"></a><ul id="zh-cn_topic_0000001258663947_ul1643764417417"><li>功能/服务较少</li><li>目标客户对产品的功能需求相同。</li></ul>
</td>
<td class="cellrowborder" valign="top" width="25.05%" headers="mcps1.2.5.1.3 "><a name="zh-cn_topic_0000001258663947_ul3302183119426"></a><a name="zh-cn_topic_0000001258663947_ul3302183119426"></a><ul id="zh-cn_topic_0000001258663947_ul3302183119426"><li>由于每个租户的费用固定，可以准确评估收入。</li><li>由于每租户费用统一，降低推广难度。</li></ul>
<p id="zh-cn_topic_0000001258663947_p133021317429"><a name="zh-cn_topic_0000001258663947_p133021317429"></a><a name="zh-cn_topic_0000001258663947_p133021317429"></a></p>
</td>
<td class="cellrowborder" valign="top" width="25%" headers="mcps1.2.5.1.4 "><a name="zh-cn_topic_0000001258663947_ul03021315426"></a><a name="zh-cn_topic_0000001258663947_ul03021315426"></a><ul id="zh-cn_topic_0000001258663947_ul03021315426"><li>由于费用固定，租户消耗资源量越大时，则盈利水平越低。</li><li>由于统一收费，对于资源使用量大的用户，难以收取更多费用。</li><li>由于统一收费，有的租户会觉得便宜，有的租户会觉得贵，难以满足不同的租户。</li></ul>
</td>
</tr>
</tbody>
</table>

## 折扣计费策略<a name="zh-cn_topic_0000001258663947_section15757858103217"></a>

>![](public_sys-resources/icon-note.gif) **说明：** 
>如果SaaS公司采用折扣计费策略，就一定要严格管理，同一个行业或相同采购量的客户折扣率差别不能太大，否则会让客户产生受欺骗的感觉。

当您已经选择了适合的计费策略后，为了更好的开展商业活动，您可以根据实际情况，设置费用折扣以激励用户，折扣主要包括如下两种类型：

-   所有用户相同折扣率：对用户数量进行分层，不同层级对应不同的折扣，所有用户享受相同的折扣。该计费策略随着用户数量的增加时，每个用户获得的折扣会越来越多。

    **表 2**  折扣率举例说明

    <a name="zh-cn_topic_0000001258663947_table23821593351"></a>
    <table><thead align="left"><tr id="zh-cn_topic_0000001258663947_row17383155913355"><th class="cellrowborder" valign="top" width="50%" id="mcps1.2.3.1.1"><p id="zh-cn_topic_0000001258663947_p17383125917357"><a name="zh-cn_topic_0000001258663947_p17383125917357"></a><a name="zh-cn_topic_0000001258663947_p17383125917357"></a>用户数</p>
    </th>
    <th class="cellrowborder" valign="top" width="50%" id="mcps1.2.3.1.2"><p id="zh-cn_topic_0000001258663947_p20383125913516"><a name="zh-cn_topic_0000001258663947_p20383125913516"></a><a name="zh-cn_topic_0000001258663947_p20383125913516"></a>折扣率</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="zh-cn_topic_0000001258663947_row3383195919355"><td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.1 "><p id="zh-cn_topic_0000001258663947_p3383205917357"><a name="zh-cn_topic_0000001258663947_p3383205917357"></a><a name="zh-cn_topic_0000001258663947_p3383205917357"></a>用户数 &lt; 50</p>
    </td>
    <td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.2 "><p id="zh-cn_topic_0000001258663947_p173833595355"><a name="zh-cn_topic_0000001258663947_p173833595355"></a><a name="zh-cn_topic_0000001258663947_p173833595355"></a>9.5折</p>
    </td>
    </tr>
    <tr id="zh-cn_topic_0000001258663947_row1338313592353"><td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.1 "><p id="zh-cn_topic_0000001258663947_p33831059173510"><a name="zh-cn_topic_0000001258663947_p33831059173510"></a><a name="zh-cn_topic_0000001258663947_p33831059173510"></a>50 ≤ 用户数 &lt;100</p>
    </td>
    <td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.2 "><p id="zh-cn_topic_0000001258663947_p2383759143510"><a name="zh-cn_topic_0000001258663947_p2383759143510"></a><a name="zh-cn_topic_0000001258663947_p2383759143510"></a>9折</p>
    </td>
    </tr>
    <tr id="zh-cn_topic_0000001258663947_row2119131919414"><td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.1 "><p id="zh-cn_topic_0000001258663947_p3119419104113"><a name="zh-cn_topic_0000001258663947_p3119419104113"></a><a name="zh-cn_topic_0000001258663947_p3119419104113"></a>100 ≤ 用户数</p>
    </td>
    <td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.2 "><p id="zh-cn_topic_0000001258663947_p7119121914412"><a name="zh-cn_topic_0000001258663947_p7119121914412"></a><a name="zh-cn_topic_0000001258663947_p7119121914412"></a>8折</p>
    </td>
    </tr>
    </tbody>
    </table>

    如果每个用户收费30元，假设某租户有120个用户，则该租户需求付费：120\*30\*0.8=2882（元）

-   每用户层相同折扣率：对用户数量进行分层，不同层级对应不同的折扣率，所有用户只享受本层级对应的折扣。该计费策略随着用户数量的增加时，每个用户只享受对应层级的折扣。

    **表 3**  折扣率举例说明

    <a name="zh-cn_topic_0000001258663947_table667022117473"></a>
    <table><thead align="left"><tr id="zh-cn_topic_0000001258663947_row1067012115475"><th class="cellrowborder" valign="top" width="50%" id="mcps1.2.3.1.1"><p id="zh-cn_topic_0000001258663947_p14670142174717"><a name="zh-cn_topic_0000001258663947_p14670142174717"></a><a name="zh-cn_topic_0000001258663947_p14670142174717"></a>用户数</p>
    </th>
    <th class="cellrowborder" valign="top" width="50%" id="mcps1.2.3.1.2"><p id="zh-cn_topic_0000001258663947_p767092111472"><a name="zh-cn_topic_0000001258663947_p767092111472"></a><a name="zh-cn_topic_0000001258663947_p767092111472"></a>折扣率</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="zh-cn_topic_0000001258663947_row14670102174712"><td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.1 "><p id="zh-cn_topic_0000001258663947_p767112118473"><a name="zh-cn_topic_0000001258663947_p767112118473"></a><a name="zh-cn_topic_0000001258663947_p767112118473"></a>第1个到第49个用户</p>
    </td>
    <td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.2 "><p id="zh-cn_topic_0000001258663947_p46717213475"><a name="zh-cn_topic_0000001258663947_p46717213475"></a><a name="zh-cn_topic_0000001258663947_p46717213475"></a>9.5折</p>
    </td>
    </tr>
    <tr id="zh-cn_topic_0000001258663947_row5671421164716"><td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.1 "><p id="zh-cn_topic_0000001258663947_p176711621184711"><a name="zh-cn_topic_0000001258663947_p176711621184711"></a><a name="zh-cn_topic_0000001258663947_p176711621184711"></a>第50个到第99个用户</p>
    </td>
    <td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.2 "><p id="zh-cn_topic_0000001258663947_p1671021164719"><a name="zh-cn_topic_0000001258663947_p1671021164719"></a><a name="zh-cn_topic_0000001258663947_p1671021164719"></a>9折</p>
    </td>
    </tr>
    <tr id="zh-cn_topic_0000001258663947_row167116216476"><td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.1 "><p id="zh-cn_topic_0000001258663947_p3671721124720"><a name="zh-cn_topic_0000001258663947_p3671721124720"></a><a name="zh-cn_topic_0000001258663947_p3671721124720"></a>第100个用户及以上</p>
    </td>
    <td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.2 "><p id="zh-cn_topic_0000001258663947_p2671102118479"><a name="zh-cn_topic_0000001258663947_p2671102118479"></a><a name="zh-cn_topic_0000001258663947_p2671102118479"></a>8折</p>
    </td>
    </tr>
    </tbody>
    </table>

    如果每个用户收费30元，假设某租户有120个用户，则该租户需求付费：49\*30\*0.95+50\*30\*0.9+21\*30\*0.8=3250.5（元）


## 总结<a name="zh-cn_topic_0000001258663947_section1118104119234"></a>

计费策略直接影响公司盈利水平，因此在公司经营过程中，建议定期审视计费策略，以便随时根据市场情况进行调整，提升公司产品的竞争力。

华为云计费策略请参考[华为云定价](https://www.huaweicloud.com/intl/zh-cn/product/price.html)。

