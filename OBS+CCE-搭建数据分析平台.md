# “OBS+CCE”搭建数据分析平台<a name="ZH-CN_TOPIC_0000001271096472"></a>

下图是显示了华为云部分大数据处理的解决方案，以[对象存储服务OBS](https://support.huaweicloud.com/intl/zh-cn/productdesc-obs/zh-cn_topic_0045829060.html)作为数据湖存储底座，以[云容器引擎CCE](https://support.huaweicloud.com/intl/zh-cn/productdesc-cce/cce_productdesc_0001.html)作为大数据集群资源调度和资源管理系统的架构图。参考：[数据分析平台搭建](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-analytics-reference-architecture)

![](figures/zh-cn_image_0000001322072337.png)

-   批处理和流处理。一般批处理的数据量很大，需要持续运行的作业算子来处理文件数据。流处理对接消息缓存队列，接入实时数据，流处理引擎通过ETL过滤清洗数据，计算引擎计算处理数据和存储处理后用于OLAP分析的数据。
-   流处理计算引擎选择：Spark是一种快速、通用的、基于内存的大数据计算引擎，SparkStreaming 是微批处理数据吞吐量大； Flink是为分布式、高性能、基于内存的大数据计算引擎。Flink的计算是事件驱动型，从事件流提取数据，根据事件触发计算、状态更新。flink吞吐量高，延时度低,计算性能更加好。
-   大数据处理方案在华为智能数据湖FusionInsight平台使用数据湖探索 DLI服务，提供一站式的流处理、批处理、交互式分析的Serverless融合处理分析服务，使用标准SQL、Spark、Flink程序就可轻松完成多数据源的联合计算分析。
-   华为MapReduce服务群集中或者自定义Hadoop集群中提交[MapReduce](https://support.huaweicloud.com/intl/zh-cn/usermanual-mrs/mrs_01_0052.html)  、[Spark](https://support.huaweicloud.com/intl/zh-cn/usermanual-mrs/mrs_01_0524.html)、[Flink](https://support.huaweicloud.com/intl/zh-cn/usermanual-mrs/mrs_01_0527.html)作业。

