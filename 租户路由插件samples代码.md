# 租户路由插件samples代码<a name="ZH-CN_TOPIC_0000001321736297"></a>

[示例代码项目参考](https://gitee.com/HuaweiCloudDeveloper/saas-housekeeper)

## schema数据路由代码片段<a name="zh-cn_topic_0000001258783901_section162472398144"></a>

1.  http拦截：

    ```
    /**
      * 通过http拦截获取租户标识
      */
     public class RequestDomainInterceptor implements HandlerInterceptor {
          @Override // 截取当前租户标识并把它放到当前线程中 ---修改1.0 
        public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
             String tenantDomain = request.getHeader("tenantDomain");
             TenantContext.set(tenantDomain);
             return true;
         }
          @Override
         // 请求完成后移除当前请求信息
         public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
             TenantContext.remove();
         }
     }
    ```


1.  添加web拦截器

    ```
    public class SpringMVCInterceptor implements WebMvcConfigurer {
          HandlerInterceptor requestDomainInterceptor;
          public SpringMVCInterceptor(HandlerInterceptor requestDomainInterceptor) {
             this.requestDomainInterceptor = requestDomainInterceptor;
         }
          @Override
         public void addInterceptors(InterceptorRegistry registry) {
             registry.addInterceptor(this.requestDomainInterceptor)
                     .addPathPatterns("/**")
                     .excludePathPatterns("*.js", "*.jpg", "*.gif", "*.css", "*.png", "*.ico");
         }
     }
    ```


1.  线程隔离存储租户标识：

    ```
    /**
      * 以ThreadLocal 保存当前租户标识
      */
     public class TenantContext {
         private static final HystrixRequestValuableDefault<String> TENANT_KEY = new HystrixRequestValuableDefault<>(); 
         public static String get() { 
            return TENANT_KEY.get();
         }
          public static void set(String key) {
             TENANT_KEY.set(key);
         }
          public static void remove() {
             TENANT_KEY.remove();
         }
     }
    ```


1.  map核对租户标识获取schema

    ```
    // 从配置获取租户信息 通过自动装配类注入 修改1.0
     @ConfigurationProperties(prefix = "spring.schema")
     @RefreshScope
     public class TenantConfigurer {
          private Map<String, String> tenantMap; 
         public Map<String, String> getTenantMap() { return tenantMap;
         }
          public void setTenantMap(Map<String, String> tenantMap) {
             this.tenantMap = tenantMap;
         }
          // 获取schema ---修改1.1
         public String getSchema() {
             String schema = null;
             try {
                 schema = tenantMap.get(TenantContext.get());
             } catch (Exception e) {
                 e.printStackTrace();
             }
             return schema;
         }
     }
    ```


1.  mybatis拦截，获取connection切换schema

    ```
    /**
      * 拦截StatementHandler,获取connection
      */
     @Intercepts({@Signature(type = StatementHandler.class, method = "prepare", args = {Connection.class, Integer.class})})
     public class MybatisInterceptor implements Interceptor {
         @Autowired
         TenantConfigurer tenantConfigurer;
          @Override // 修改1.0
         public Object intercept(Invocation invocation) throws Throwable {
              Connection coon = (Connection) invocation.getArgs()[0];
             String schema = tenantConfigurer.getSchema();
             if (schema != null) {
                 coon.setCatalog(schema);
             }
             return invocation.proceed();
         }
     }
    ```


1.  定义自动装配类（新增）

    ```
    @Configuration
     @EnableConfigurationProperties(value = TenantConfigurer.class)
     public class TenantAutoConfiguration {
          @Bean
         public MybatisInterceptor mybatisInterceptor(){ 
            return new MybatisInterceptor();
         }
          @Bean
         @ConditionalOnWebApplication
         public SpringMVCInterceptor springMVCInterceptor() {
             return new SpringMVCInterceptor(requestDomainInterceptor());
         }
          @Bean //注入拦截器-- 修改1.0
         public HandlerInterceptor requestDomainInterceptor() {
             return new RequestDomainInterceptor();
         } }
    ```


微服务间调用带上租户标识

使用feign来实现微服务之间调用，实现RequestIntercepter接口, 重写方法

```
@Override
 public void apply(RequestTemplate template){
     template.addHeader("tenantDomain",TenantContext.get());
```

自动装配实现

```
保存在resources目录下META-INF的spring.factories文件中： 修改1.0（直接装配TenantAutoConfiguration即可）
 org.springframework.boot.autoconfigure.EnableAutoConfiguration=com.huawei.saashousekeeper.config.TenantAutoConfiguration
```

路由的使用

```
<dependency>
         <groupId>com.huawei.saas-housekeeper</groupId>
         <artifactId>saas-housekeeper-tenant-router-starter</artifactId>
         <version>0.0.1-SNAPSHOT</version>
 </dependency> 
 
```

以上代码仅用于展示租户传递与schema路由的示例，功能完善的路由插件请参考[多租户数据路由插件](https://gitee.com/HuaweiCloudDeveloper/saas-tenant-router-starter)。

